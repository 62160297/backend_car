package com.example.cardealer.dao;

import java.math.BigInteger;

import java.util.List;

import com.example.cardealer.model.Car;

public interface CarDao {
    int save(Car car);

    List<Car> findAll();

    Car findById(BigInteger id);

    int update(Car car, BigInteger id);

    int deleteById(BigInteger id);

    List<Car> search(String str);
}
