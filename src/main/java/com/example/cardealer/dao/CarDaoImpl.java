package com.example.cardealer.dao;

import java.math.BigInteger;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.cardealer.model.Car;

@Repository
public class CarDaoImpl implements CarDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

	private final String ID = "id";
	private final String MAKE = "make";
	private final String MODEL = "model";
	private final String TYPE = "type";
	private final String TRANSMISSION = "transmission";
	private final String COLOR = "color";
	private final String YEAR = "year";
	private final String MILES = "miles";
	private final String PRICE = "price";

	final RowMapper<Car> ROW_MAPPER = (ResultSet rs, int rowNum) -> {
		final Car mapperObject = new Car();
		mapperObject.setId(BigInteger.valueOf(rs.getLong(ID)));
		mapperObject.setMake(rs.getString(MAKE));
		mapperObject.setModel(rs.getString(MODEL));
		mapperObject.setType(rs.getString(TYPE));
		mapperObject.setTransmission(rs.getString(TRANSMISSION));
		mapperObject.setColor(rs.getString(COLOR));
		mapperObject.setYear(rs.getString(YEAR));
		mapperObject.setMiles(rs.getFloat(MILES));
		mapperObject.setPrice(rs.getFloat(PRICE));

		return mapperObject;

	};


    @Override
    public int save(Car car) {
		int result = 0;
		try{
			result = jdbcTemplate.update(
					"INSERT INTO cars (make, model, type, transmission, color, year, miles, price) VALUES(?,?,?,?,?,?,?,?)",
					new Object[] {
							car.getMake(),
							car.getModel(),
							car.getType(),
							car.getTransmission(),
							car.getColor(),
							car.getYear(),
							car.getMiles(),
							car.getPrice(),
					});
		}catch (Exception e){
			System.out.println(e);
			throw e;
		}
		return result;
    }

	@Override
	public List<Car> findAll() {
		List<Car> resultList;
		try{
			resultList = jdbcTemplate.query("SELECT * FROM cars", ROW_MAPPER);
		}catch (Exception e){
			System.out.println(e);
			throw e;
		}

		return resultList;
	}

	@Override
	public Car findById(BigInteger id) {
		Car result = new Car();
		try{
			result = jdbcTemplate.queryForObject("SELECT * FROM cars WHERE id=?", ROW_MAPPER, id);
		}catch (Exception e){
			System.out.println(e);
			throw e;
		}
		return result;
	}
	@Override
	public int update(Car car, BigInteger id) {
		int result = 0;
		try{
			result = jdbcTemplate.update("UPDATE cars SET make = ?, model = ?, type = ?, transmission = ?, color = ?, year = ?, miles = ?, price = ? WHERE id = ?",
					new Object[] {
							car.getMake(),
							car.getModel(),
							car.getType(),
							car.getTransmission(),
							car.getColor(),
							car.getYear(),
							car.getMiles(),
							car.getPrice(),
							id
					});
		}catch (Exception e){
			System.out.println(e);
			throw e;
		}
		return result;
	}

	@Override
    public int deleteById(BigInteger id) {
		int result = 0;
		try {
			result = jdbcTemplate.update("DELETE FROM cars WHERE id=?", id);
		}catch (Exception e){
			System.out.println(e);
			throw e;
		}
		return result;
    }

	@Override
	public List<Car> search(String str){
		List<Car> resultList;
		try {
			resultList = jdbcTemplate.query("SELECT * FROM cars \n"+
							"WHERE make LIKE ? OR model LIKE ? \n" +
							"OR type LIKE ? OR transmission LIKE ? \n"+
							"OR color LIKE ? OR year LIKE ? \n"+
							"OR miles LIKE ? OR price LIKE ? \n", ROW_MAPPER,
					new Object[]{"%"+str+"%","%"+str+"%","%"+str+"%","%"+str+"%","%"+str+"%","%"+str+"%","%"+str+"%","%"+str+"%"});
		}catch (Exception e){
			System.out.println(e);
			throw e;
		}
		return resultList;
	}
}
