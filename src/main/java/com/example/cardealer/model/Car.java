package com.example.cardealer.model;

import java.math.BigInteger;

public class Car {
    private BigInteger id;
    private String make;
    private String model;
    private String type;
    private String transmission;
    private String color;
    private String year;
    private Float miles;
    private Float price;

    public BigInteger getId() {
        return id;
    }
    public void setId(BigInteger id) {
        this.id = id;
    }
    public String getMake() {
        return make;
    }
    public void setMake(String make) {
        this.make = make;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }


    public String getTransmission() {
        return transmission;
    }
    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public String getYear() {
        return year;
    }
    public void setYear(String year) {
        this.year = year;
    }
    public Float getMiles() {
        return miles;
    }
    public void setMiles(Float miles) {
        this.miles = miles;
    }
    public Float getPrice() {
        return price;
    }
    public void setPrice(Float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Car{id=").append(id);
        sb.append(", make=").append(make);
        sb.append(", model=").append(model);
        sb.append(", type=").append(type);
        sb.append(", transmission=").append(transmission);
        sb.append(", color=").append(color);
        sb.append(", year=").append(year);
        sb.append(", miles=").append(miles);
        sb.append(", price=").append(price);
        sb.append("}");
        return sb.toString();
    }
}