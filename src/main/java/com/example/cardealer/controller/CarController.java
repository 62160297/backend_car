package com.example.cardealer.controller;

import java.math.BigInteger;
import java.util.List;

import com.example.cardealer.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.cardealer.model.Car;
import com.example.cardealer.dao.CarDao;

@CrossOrigin(origins = "http://localhost:8081/")
@RestController
@RequestMapping("/api")
public class CarController  extends CarService {
    Car car = new Car();

    @Autowired
    private CarDao carDao;

    @PostMapping("/getCars")
    public List<Car> findAll(@RequestParam(value = "search", defaultValue = "")String str) {
        if(!str.isEmpty()){
            return carDao.search(str);
        }
        return carDao.findAll();
    }

    @PostMapping("/addCar")
    public String save(@RequestBody Car car) {
        if (validateNotNull(car.getMake()) && validateNotNull(car.getModel()) && validateNotNull(car.getType()) && validateNotNull(car.getTransmission())
                && validateNotNull(car.getColor()) && validateNotNull(car.getYear()) && car.getMiles() > 0 && car.getPrice() > 0) {
            return carDao.save(car) + " Save car Success";
        } else {
            return false + " Save car Fails";
        }
    }
//    @PostMapping("/cars/{id}")
//    public Car findById(@PathVariable BigInteger id) {
//        return carDao.findById(id);
//    }

    @PostMapping("/editCar/{id}")
	public String update(@RequestBody Car car, @PathVariable BigInteger id) {
        if (validateNotNull(car.getMake()) && validateNotNull(car.getModel()) && validateNotNull(car.getType()) && validateNotNull(car.getTransmission())
                && validateNotNull(car.getColor()) && validateNotNull(car.getYear()) && car.getMiles() > 0 && car.getPrice() > 0) {
            return carDao.update(car, id) + " update car Success";
        } else {
            return false + " update car Fails";
        }
    }

    @PostMapping("/deleteCar/{id}")
	public String deleteById(@PathVariable BigInteger id) {
		return carDao.deleteById(id)+" Car(s) delete from the database";
	}
}
